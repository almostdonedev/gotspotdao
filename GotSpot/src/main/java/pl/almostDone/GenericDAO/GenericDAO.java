package pl.almostDone.GenericDAO;

public interface GenericDAO<T> {

	public T save(T entity);
	public Boolean delete (T entity);
	public T edit (T entity);
	public T find (Long empId);
	public Long getCount();
	
}
