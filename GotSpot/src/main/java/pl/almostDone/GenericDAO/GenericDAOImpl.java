package pl.almostDone.GenericDAO;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class GenericDAOImpl<T> implements GenericDAO<T> {

	protected EntityManager entityManager;
	private Class<T> type;

	public GenericDAOImpl() {

	}
	


	public GenericDAOImpl(Class<T> type) {
		this.type = type;
	}

	@PersistenceContext(unitName = "GotSpotPU")
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public T save(T entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.flush();
		entityManager.getTransaction().commit();
		return entity;
	}

	public Boolean delete(T entity) {
		try {
			entityManager.remove(entity);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public T edit(T entity) {
		try {
			return entityManager.merge(entity);
		} catch (Exception e) {
			return null;
		}
	}

	public T find(Long entityId) {
		return (T)entityManager.find(type.getClass(), entityId ); 
	}

	public Long getCount() {
		
		StringBuilder queryString = new StringBuilder("SELECT count(o) from ");
		
		queryString.append(type.getSimpleName()).append(" o ");
		
		Query query = this.entityManager.createQuery(queryString.toString());
		
		return (Long) query.getSingleResult();

	}

}
