package pl.almostDone.DAO.Interfaces;

import pl.almostDone.Exceptions.CityExistsInDBException;
import pl.almostDone.entities.City;

public interface CityDAO {

	public City addCity(City city) throws CityExistsInDBException;
	public City getCityByName(City city) throws CityExistsInDBException;
}
