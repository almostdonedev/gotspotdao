package pl.almostDone.Exceptions;

public class UserNotExistInDB extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3756139030925446814L;
	private String username;

	public UserNotExistInDB(String username, String message) {
		super(message);
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
	
	
	
}
