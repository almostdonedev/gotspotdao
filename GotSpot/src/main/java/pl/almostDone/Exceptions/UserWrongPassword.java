package pl.almostDone.Exceptions;

import pl.almostDone.entities.User;

public class UserWrongPassword extends Exception {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8919398934680679467L;
	private User user;

	public UserWrongPassword(User user, String message) {
		super(message);
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	
}
