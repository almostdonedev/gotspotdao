package pl.almostDone.entities;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "spoty")
public class Spot {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "spot_id")
	private Long id;
	private String name;
	private String description;
	private Category category;
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name = "rating_id", referencedColumnName = "spot_id")
	private List<Rating> allRatings;
	private Double avgRating;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User addedBy;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="city_id")
	private City city;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="address_id")
	private Address address;
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name = "comment_id", referencedColumnName = "spot_id")
	private List<Comment> allComments;
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name = "photo_id", referencedColumnName = "spot_id")
	private List<Photo> allPhotos;
	private Date added;
	
	
	
	public enum Category {
		SPOT("Spot"), SKATEPARK("Skatepark"), SKATEPLAZA("Skateplaza");
		
		private String category;
		
		private Category(String category) {
			this.category = category;
		}
	}
}
