package pl.almostDone.DAO.unit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.configuration.MockAnnotationProcessor;

import pl.almostDone.DAO.impl.CityDAOImpl;
import pl.almostDone.Exceptions.CityExistsInDBException;
import pl.almostDone.entities.City;

import static org.mockito.Mockito.*;
public class CityDAOImplTest {

	
	@Mock
	CityDAOImpl cityDao = new CityDAOImpl();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = CityExistsInDBException.class)
	public void testAddCity() throws CityExistsInDBException {
		
		City city = new City("Testowe", "Dzielnia");
		
		when(cityDao.getCityByName(city)).thenReturn(new City("test", "lol"));
		when(cityDao.addCity(city)).thenCallRealMethod();
		
		City addedCity = cityDao.addCity(city);
		
	}

}
