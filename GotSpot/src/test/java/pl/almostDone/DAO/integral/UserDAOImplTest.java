package pl.almostDone.DAO.integral;

import static org.junit.Assert.*;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.almostDone.DAO.impl.CityDAOImpl;
import pl.almostDone.DAO.impl.UserDAOImpl;
import pl.almostDone.Exceptions.UserExistsInDBException;
import pl.almostDone.Exceptions.UserNotExistInDB;
import pl.almostDone.Exceptions.UserWrongPassword;
import pl.almostDone.entities.City;
import pl.almostDone.entities.User;

public class UserDAOImplTest {

	static UserDAOImpl userDao = null;
	static CityDAOImpl cityDaoImpl = null;
	static EntityManagerFactory emf = Persistence.createEntityManagerFactory("GotSpotPUTest");
	static EntityManager em = emf.createEntityManager();
	
	@BeforeClass
	public static void setUp() throws Exception {
		userDao = new UserDAOImpl();
		userDao.setEntityManager(em);
		
		cityDaoImpl = new CityDAOImpl();
		cityDaoImpl.setEntityManager(em);
		
		User user1 = new User("Pawulo", "kochamCyce", new City("Rybnik", ""));
		User user2 = new User("Hubert", "kebabMasha", new City("Warszawa", "Bemowo"));
		
		em.getTransaction().begin();
		em.persist(user1);
		em.persist(user2);
		em.getTransaction().commit();
		
		
	}

	@AfterClass
	public static void tearDown() throws Exception {
		em.close();
		emf.close();
	}

	@Test
	public void test_addUser_Users_City_Exist_IN_DB_Should_NOT_ADD_CITY() throws UserExistsInDBException {
		User pawel = new User("Bombello", "MafiaKurwa", new City("Rybnik", ""));
		
		UserDAOImpl daoImpl = new UserDAOImpl();
		daoImpl.setEntityManager(em);
		
		Long usersPrev = userDao.getUsersCount();

		
		Long prev = cityDaoImpl.getCitiesCount();
		
		User addedUser1 = daoImpl.addUser(pawel);
		
		Long now = cityDaoImpl.getCitiesCount();
		assertEquals(prev, now);
		
		Long usersNow = userDao.getUsersCount();
		assertEquals((Long) (usersPrev+1), usersNow);
		
		
	}
	
	@Test
	public void test_addUser_Users_City_Not_Exist_IN_DB_Should_ADD_CITY() throws UserExistsInDBException {
		User pawel = new User("Ćpun", "MafiaKurwa", new City("Warszawa", "Włochy"));
		
		UserDAOImpl daoImpl = new UserDAOImpl();
		daoImpl.setEntityManager(em);
		
		Long usersPrev = daoImpl.getUsersCount();

	
		Long prev = cityDaoImpl.getCitiesCount();
		
		User addedUser1 = daoImpl.addUser(pawel);
		
		Long now = cityDaoImpl.getCitiesCount();
		assertEquals(prev,(Long) (now-1));
		
		Long usersNow = daoImpl.getUsersCount();
		
		assertEquals((Long) (usersNow-1), usersPrev );
		
	}
	
	@Test( expected = UserExistsInDBException.class)
	public void test_addUser_Username_Exists_In_DB() throws UserExistsInDBException {
		User pawel = new User("Pawulo", "MafiaKurwa", new City("Warszawa", "Włochy"));
		
		UserDAOImpl daoImpl = new UserDAOImpl();
		daoImpl.setEntityManager(em);
		
		Long usersPrev = daoImpl.getUsersCount();
		
		daoImpl.addUser(pawel);
		
		Long useresNow = daoImpl.getUsersCount();
		
		assertEquals(useresNow, usersPrev);
		

	}

	@Test(expected = UserNotExistInDB.class)
	public void test_checkLoginData_User_Not_Exist_In_Database() throws UserNotExistInDB, UserWrongPassword {
		String username = "Cpunello";
		String password = "SowaMistrzu";
		
		userDao = new UserDAOImpl();
		userDao.setEntityManager(em);
		
		User checkLoginData = userDao.checkLoginData(username, password);
		assertNull(checkLoginData);
	}

	@Test(expected = UserWrongPassword.class)
	public void test_checkLoginData_User_Wrong_Password() throws UserNotExistInDB, UserWrongPassword {
		String username = "Pawulo";
		String password = "SowaMistrzu";
		
		userDao = new UserDAOImpl();
		userDao.setEntityManager(em);
		
		User checkLoginData = userDao.checkLoginData(username, password);
		assertNull(checkLoginData);
	}

	@Test
	public void test_checkLoginData_User_Loggin_Correctly() throws UserNotExistInDB, UserWrongPassword {
		String username = "Pawulo";
		String password = "kochamCyce";
		
		userDao = new UserDAOImpl();
		userDao.setEntityManager(em);
		
		User checkLoginData = userDao.checkLoginData(username, password);
		assertNotNull(checkLoginData);
	}
}
